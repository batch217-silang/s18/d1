// console.log('hello')
// FUNCTIONS

// PARAMETERS AND ARGUMENTS

// function printInput(){
// 	let nickname = prompt("Enter your nickname");
// 	console.log("Hi, " + nickname);
// }
// printInput()

/*This function has parameter and arguments*/
function printName(name){
	console.log("My name is " + name)
}
printName('Juana')

/*
1. printName(Juana) - ARGUMENT
2. will be stored to our PARAMETER
3. The data stored in PARAMETER can be used to the code block inside the function
*/

function printName(name){
	console.log("My name is " + name)
}
printName('Cely')

let sampleVariable = "Yui";
printName(sampleVariable);

/*Function arguments cannot be used by a function if there are no paramenters provide within the function*/

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log('The remainder of ' + num + ' divided by 8 is: ' + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(32);
/*You can also do the same using promt () howver take note that promt outputs a strings. Strings are not ideal for mathematical computations*/

// FUNCTIONS AS ARGUMENTS
/*-can */

function argumentFunctions() {
	console.log("This function was passed as an argument before the message.")
}

function invokeFunction(argumentFunctions){
	argumentFunctions();
}
invokeFunction(argumentFunctions);
console.log(argumentFunctions);

// MULTIPLE PARAMETERS
function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

createFullName("Cely", "Coyutan", "Silang");
createFullName("Cely", "Coyutan", "");


// MULTIPLE PARAMETER using stored data in a variable 
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";
createFullName(firstName,middleName,lastName);

// 
function printFullName(middleName, firstName, lastName){
	console.log(firstName + ' ' + middleName + " " + lastName);
}
printFullName("Juan", "Dela", "Cruz");


// RETURN STATEMENT
// passed to line/block of code
function returnFullName(firstName, middleName, lastName){
	console.log("Test Console");
	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("This will not be listed in the console");
	}

let completeName = returnFullName("Juan", "Dela", "Cruz");
console.log(completeName);


console.log(returnFullName(firstName, middleName, lastName));
function returnAddress(city, country){
	let fullAddress = city + "," + country;
	return fullAddress;
	console.log("This will not be displayed");
}
let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);

function printPlayerInfo(username, level, job){
	// console.log("Username: " + username);
	// console.log("Level: " + level);
	// console.log("Job: " + job);
	return "Username: " + username;
}
let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
console.log(user1);


// user1 is undefined because printplayer returns nothing
// function printPlayerInfo(username, level, job){
	// console.log("Username: " + username);
	// console.log("Level: " + level);
	// console.log("Job: " + job);


// let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
// console.log(user1); 